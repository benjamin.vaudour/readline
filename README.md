# readline

Readline est un prompt Linux qui se veut similaire à C readline mais en ligne de commande.

## Utilisation typique

readline peut être utilisé de la façon suivante :

```go
package main

import (
    "fmt"
    "os"
    "framagit.org/benjamin.vaudour/readline"
)

func main() {
    prompt := "\033[1;31m> \033[m"
    rl := readline.New()
    defer readline.Close()
    for {
        input, err := rl.Prompt(prompt) // rl.PromptPassword(prompt) pour masquer ce qui est saisi
        if err != nil {
            fmt.Printf("\033[1,31mErreur système : %s\033[m\n", err)
            os.Exit(1)
        } else {
            //@TODO Do some stuff with input
            if result == "exit" {
                return
            }
        }
    }
}
```



## Raccourcis clavier <u>disponibles</u>

| Raccourci clavier                      | Action                                                       |
| -------------------------------------- | ------------------------------------------------------------ |
| Ctrl+A, Home                           | Retour au début de la saisie                                 |
| Ctrl+E, End                            | Retour à la fin de la saisie                                 |
| Ctrl+B, Left                           | Déplacement d’un caractère vers la gauche                    |
| Ctrl+F, Right                          | Déplacement d’un caractère vers la droite                    |
| Alt+B, Ctrl+Left                       | Déplacement d’un mot vers la gauche                          |
| Alt+F, Ctrl+Right                      | Déplacement d’un mot vers la droite                          |
| Ctrl+U                                 | Suppression de tous les caractères du début de la ligne au curseur (curseur non compris) |
| Ctrl+K                                 | Suppression de tous les carctères du curseur (compris) à la fin de la ligne |
| Ctrl+D (si saisie commencée), Del      | Suppression du caractère sous le curseur                     |
| Ctrl+D (si ligne vide)                 | Termine l’application (EOF)                                  |
| Ctrl+H, Backspace                      | Suppression du caractère avant le curseur                    |
| Alt+D, Alt+Del                         | Suppression du prochain mot                                  |
| Alt+Backspace                          | Suppression du précédent mot                                 |
| Ctrl+C                                 | Termine l’application (avec erreur)                          |
| Ctrl+P, Up (si ligne vide au départ)   | Remonte à l’historique précédent                             |
| Ctrl+N, Down (si ligne vide au départ) | Descend dans l’historique                                    |
| Ctrl+R, Up (si ligne non vide)         | Recherche dans l’historique (par préfixe de ligne) à partir de la fin |
| Ctrl+S, Down (si ligne non vide)       | Recherche dans l’historique (par préfixe de ligne) à partir du début |
| Ctrl+Y                                 | Copie le dernier élément supprimé                            |
| Alt+Y                                  | Remonte dans l’historique des éléments supprimés et les copie (implique Ctrl+Y précédemment lancé) |
| Ctrl+G, Cancel                         | Annule les dernières actions temporaires (historique, recherche, copie, historique de copie) |



