package readline

import (
	"os"

	"framagit.org/benjamin.vaudour/shell/v2/console/atom"
)

// Readline est un terminal similaire à readline.
type Readline struct {
	st *atom.State
}

// New initialise un nouveau Readline.
func New() *Readline {
	return &Readline{
		st: atom.NewState(),
	}
}

func (rl *Readline) execChar(r atom.Char, isPassword bool) (err error, fix, stop bool) {
	if stop = r == '\n' || r == '\r'; stop {
		fix = true
		return
	}
	if atom.IsPrintable(r) {
		rl.st.Insert(r)
		fix = true
		return
	}
	var ok bool
	switch r {
	case atom.Bs, atom.C_H: // Back
		ok, fix = rl.st.Back(), true
	case atom.C_W: // Back Word
		ok, fix = rl.st.BackWord(), true
	case atom.C_D: // Del or reset line
		l := rl.st.BufferLen()
		if l == 0 {
			rl.st.Return()
			os.Exit(0)
		} else {
			ok, fix = rl.st.Del(), true
			rl.st.Restart()
		}
	case atom.C_K: // Remove EOL
		ok, fix = rl.st.RemoveEnd(), true
	case atom.C_U: // Remove BOL
		ok, fix = rl.st.RemoveBegin(), true
	case atom.C_A: // Begin of line
		ok, fix = rl.st.Begin(), true
	case atom.C_E: // End of line
		ok, fix = rl.st.End(), true
	case atom.C_B: // Left
		ok, fix = rl.st.Left(), true
	case atom.C_F: // Right
		ok, fix = rl.st.Right(), true
	case atom.C_T: // Transpose
		ok, fix = rl.st.Transpose(), true
	case atom.C_P: // Previous History
		if !isPassword {
			ok = rl.st.HistoryPrev()
		}
	case atom.C_N:
		if !isPassword {
			ok = rl.st.HistoryNext()
		}
	case atom.C_S: // Search History
		if !isPassword {
			ok = rl.st.SearchHistoryNext()
		}
	case atom.C_R: // Reverse SearchHistory
		if !isPassword {
			ok = rl.st.SearchHistoryPrev()
		}
	case atom.C_G, atom.Esc: // Cancel history
		ok, fix = rl.st.Cancel(), true
	case atom.C_Y: // Yank
		ok = rl.st.Yank()
	case atom.C_C: // Emergency stop
		rl.st.Return()
		os.Exit(1)
	}
	if !ok {
		rl.st.Beep()
	}
	return
}

func (rl *Readline) execSequence(s atom.Sequence, isPassword bool) (err error, fix, stop bool) {
	var ok bool
	switch s {
	case atom.Up: // Previous history
		if !isPassword {
			l := rl.st.BufferLen()
			if l == 0 || rl.st.IsHistoryMode() {
				ok = rl.st.HistoryPrev()
			} else {
				ok = rl.st.SearchHistoryPrev()
			}
		}
	case atom.Down: // Next history
		if !isPassword {
			l := rl.st.BufferLen()
			if l == 0 || rl.st.IsHistoryMode() {
				ok = rl.st.HistoryNext()
			} else {
				ok = rl.st.SearchHistoryNext()
			}
		}
	case atom.Right: // Move right
		ok, fix = rl.st.Right(), true
	case atom.Left: // Move Left
		ok, fix = rl.st.Left(), true
	case atom.Ins: // Toggle insert
		rl.st.ToggleReplace()
		ok = true
	case atom.Del: // delete under cursor
		ok, fix = rl.st.Del(), true
	case atom.End: // go to end
		ok, fix = rl.st.End(), true
	case atom.Home: // go to begin
		ok, fix = rl.st.Begin(), true
	case atom.A_Bs: // delete begin of word
		ok, fix = rl.st.BackWord(), true
	case atom.A_D, atom.A_Del: // delete end of word
		ok, fix = rl.st.DelWord(), true
	case atom.C_Right, atom.A_F: // Next word
		ok, fix = rl.st.NextWord(), true
	case atom.C_Left, atom.A_B: // Begin of word
		ok, fix = rl.st.PrevWord(), true
	case atom.A_Y: // Previous yank
		ok = rl.st.YankPrev()
	}
	if !ok {
		rl.st.Beep()
	}
	return
}

func (rl *Readline) prompt(p string, isPassword bool) (result string, err error) {
	if err = rl.st.SetPrompt(p); err != nil {
		return
	}
	rl.st.Start()
	defer rl.st.Stop()
	var refresh func() error
	if isPassword {
		refresh = func() error { return rl.st.Print("", 0) }
	} else {
		refresh = func() error {
			str, cursor := rl.st.Buffer()
			return rl.st.Print(str, cursor)
		}
	}
	refresh()
	var key atom.Key
	var fix, stop bool
	for {
		if key, err = rl.st.Next(); err != nil {
			break
		}
		if key.IsSequence() {
			err, fix, stop = rl.execSequence(key.Sequence(), isPassword)
		} else {
			err, fix, stop = rl.execChar(key.Char(), isPassword)
		}
		if err == nil {
			err = refresh()
		}
		if stop || err != nil {
			break
		}
		if fix {
			rl.st.FixState()
		}
	}
	if err == nil {
		result, _ = rl.st.Buffer()
		if !isPassword && len(result) > 0 {
			rl.st.AppendHistory(result)
		}
	}
	rl.st.Clear()
	rl.st.FixState()
	rl.st.Return()
	return
}

// Prompt retourne la chaîne saisie.
func (rl *Readline) Prompt(p string) (result string, err error) {
	return rl.prompt(p, false)
}

// PromptPassword agit comme Prompt mais n’affiche pas à l’écran ce qui est saisi.
func (rl *Readline) PromptPassword(p string) (result string, err error) {
	return rl.prompt(p, true)
}

// Close ferme proprement le Readline.
func (rl *Readline) Close() error {
	return rl.st.Close()
}
